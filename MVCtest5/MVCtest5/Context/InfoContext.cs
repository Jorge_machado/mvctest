﻿using MVCtest5.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MVCtest5.Controllers
{
    public class InfoContext: DbContext
    {
        public DbSet<Region> Regions { get; set; }
        public DbSet<Municipality> Municipalities { get; set; }
        public object RegionMunicipality { get; internal set; }

        //public DbSet<RegionMunicipality> RegionMunicipality { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Region>().HasMany(
                t => t.Municipalities).WithMany(t => t.Regions)
                .Map(m => { m.ToTable("RegionMunicipality"); 
                m.MapLeftKey("RegionId"); m.MapRightKey("MunicipalityId"); }); 
                base.OnModelCreating(modelBuilder);
        }

    }
}