﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVCtest5.Models
{
    public class Municipality
    {
        [Key]
        public int MunicipalityId { get; set; }
        public string MunicipalityName { get; set;}
        public Status MunicipalityStatus { get; set; }
        public List<Region> Regions { get; set; }

        public enum Status
        { 
            Activo,
            Inactivo
        }
    }
}