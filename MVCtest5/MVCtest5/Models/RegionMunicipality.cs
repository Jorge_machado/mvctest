﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MVCtest5.Models
{
    public class RegionMunicipality
    {
        public int RegionId { get; set; }

        public int MunicipalityId { get; set; }


        [ForeignKey("RegionId")]
        public Region Region { get; set; }

        [ForeignKey("MunicipalityId")]
        public Municipality Municipality { get; set; }
    }
}