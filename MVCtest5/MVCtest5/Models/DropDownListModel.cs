﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCtest5.Models.ViewModel
{
    public class DropDownListModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}