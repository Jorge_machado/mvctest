﻿using MVCtest5.Controllers;
using MVCtest5.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCtest5.Repositories
{
    public class RepositoryMunicipality
    {
        InfoContext InfoContext;
        public RepositoryMunicipality()
        {
            this.InfoContext = new InfoContext();
        }
        public List<Municipality> GetMunicipality() {
            var municipalities = from municipality in InfoContext.Municipalities
                        select municipality;
            return municipalities.ToList();
        }
    }
}