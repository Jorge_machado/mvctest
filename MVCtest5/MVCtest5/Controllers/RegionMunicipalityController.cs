﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCtest5.Controllers
{
    public class RegionMunicipalityController : Controller
    {
        // GET: RegionMunicipality
        public ActionResult Index()
        {
            return View();
        }

        // GET: RegionMunicipality/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: RegionMunicipality/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: RegionMunicipality/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: RegionMunicipality/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: RegionMunicipality/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: RegionMunicipality/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: RegionMunicipality/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
