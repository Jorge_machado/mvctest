﻿using MVCtest5.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCtest5.Models.ViewModel;

namespace MVCtest5.Controllers
{
    public class MunicipalityController : Controller
    {
        private InfoContext db = new InfoContext();

        // GET: Municipality
        public ActionResult Index()
        {
            return View(db.Municipalities.ToList());
        }

        // GET: Municipality/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var municipality = db.Municipalities.Find(id);

            if (municipality == null)
            {
                return HttpNotFound();
            }

            return View(municipality);
        }

        // GET: Municipality/Create
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Municipality/Create
        [HttpPost]
        public ActionResult Create(Municipality municipality)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Municipalities.Add(municipality);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(municipality);
            }
            catch
            {
                return View();
            }
        }

        // GET: Municipality/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var municipality = db.Municipalities.Find(id);

            if (municipality == null)
            {
                return HttpNotFound();
            }

            return View(municipality);
        }

        // POST: Municipality/Edit/5
        [HttpPost]
        public ActionResult Edit(Municipality municipality)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (municipality.MunicipalityStatus > 0)
                    {
                        var municipalityId = municipality.MunicipalityId;
                        var st = db.Database.ExecuteSqlCommand($@"DELETE FROM RegionMunicipality WHERE MunicipalityId=" + municipalityId);

                        db.Entry(municipality).State = EntityState.Modified;
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }

                    db.Entry(municipality).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(municipality);

            }
            catch
            {
                return View(municipality);
            }
        }

        // GET: Municipality/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var municipality = db.Municipalities.Find(id);

            if (municipality == null)
            {
                return HttpNotFound();
            }

            return View(municipality);
        }

        // POST: Municipality/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, Municipality municipality)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    municipality = db.Municipalities.Find(id);

                    if (municipality == null)
                    {
                        return HttpNotFound();
                    }

                    db.Municipalities.Remove(municipality);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View(municipality);
            }
        }
    }
}
