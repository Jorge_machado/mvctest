﻿using MVCtest5.Models;
using MVCtest5.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace MVCtest5.Controllers
{
    public class RegionController : Controller
    {
        private InfoContext db = new InfoContext();

        Repositories.RepositoryMunicipality repo;

        public RegionController()
        {
            this.repo = new Repositories.RepositoryMunicipality();
        }

        public ActionResult _PartialView(int RegionId)
        {
            ViewBag.regionid = RegionId;

            var ListMunicipalities = (from Municipalities in db.Municipalities
                                  where Municipalities.Regions.Any(c => c.RegionId == RegionId)
                                  select Municipalities).ToList();

            return PartialView(ListMunicipalities);
        }

        /// GET: Region
        public ActionResult Index()
        {
            return View(db.Regions.ToList());
        }

        // GET: Region/Details/5
        public ActionResult Details(int id)
        {
            var region = db.Regions.Find(id);
            return View(region);
        }

        // GET: Region/Create
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Region/Create
        [HttpPost]
        public ActionResult Create(Region region)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Regions.Add(region);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(region);
            }
            catch
            {
                return View();
            }
        }

        // GET: Region/Edit/5
        public ActionResult Edit(int? id)
        {
            List<DropDownListModel> list = null;

            list = (from d in db.Municipalities.Where(o => o.MunicipalityStatus == 0)
                           select new DropDownListModel
                           {
                               Id = d.MunicipalityId,
                               Name = d.MunicipalityName
                           }).ToList();
            List<SelectListItem> items = list.ConvertAll(d =>
            {
                return new SelectListItem()
                {
                    Text = d.Name.ToString(),
                    Value = d.Id.ToString()
                };
            });
            ViewBag.items = items;  

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var region = db.Regions.Find(id);

            if (region == null)
            {
                return HttpNotFound();
            }

            return View(region);
        }

        // POST: Region/Edit/5
        [HttpPost]
        public ActionResult Edit(Region region, FormCollection forms, string RegionId)
        {
            ViewBag.regionid = RegionId;
            try
            {
                if (ModelState.IsValid)
                {
                    var municipalityId = forms["Combo"].ToString();

                    if (municipalityId != "")
                    {
                        var InsertedRows = db.Database.ExecuteSqlCommand($@"INSERT INTO RegionMunicipality (RegionId, MunicipalityId)
                                                                            VALUES ({region.RegionId}, {municipalityId})");
                        db.Entry(region).State = EntityState.Modified;
                        db.SaveChanges();
                        return RedirectToAction("Edit");
                    }
                    else
                    {
                        db.Entry(region).State = EntityState.Modified;
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                }
                
                return View(region);

            }
            catch(Exception ex)
            {
                //return View(region);
                return RedirectToAction("Edit");
            }
        }

        // GET: Region/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var region = db.Regions.Find(id);

            if (region == null)
            {
                return HttpNotFound();
            }

            return View(region);
        }

        // POST: Region/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, Region region, FormCollection forms)
        {
            try
            {
                if (ModelState.IsValid)
                {
                     region = db.Regions.Find(id);
                    if (region == null)
                    {
                        return HttpNotFound();
                    }

                    db.Regions.Remove(region);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View(region);
            }
        }

        [Route("UnlinkMunicipality/{id}")]
        [HttpGet] 
        public ActionResult UnlinkMunicipality(Region region, int id, Municipality municipality, int municipalityId)
        {
            var idRegion = db.Regions.Find(id);
            var regionId = idRegion.RegionId;

            var st = db.Database.ExecuteSqlCommand($@"DELETE FROM RegionMunicipality WHERE MunicipalityId=" + municipalityId + "AND RegionId=" + regionId);

            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
